# ClearVue

ClearVue is a mobile app developed in Flutter that provides a 5-day weather forecast for a chosen location.

## Getting Started

1. **Clone the project to your local machine:**

````markdown

git clone https://gitlab.com/gitlab7504202/clearvue.git

````

2. **Navigate to the project directory:**

   ```bash
   cd clearvue
   ```

3. **Ensure you have Flutter and Dart installed.** If not, refer to the [official Flutter installation guide](https://flutter.dev/docs/get-started/install).

4. **Install dependencies:**

   ```bash
   flutter pub get
   ```

5. **Run the app:**
   ```bash
   flutter run
   ```

## Additional info

The app uses openweather api to fetch weather info and by default it fetches info for Mumbai.

## Additional Resources

- [Flutter Documentation](https://docs.flutter.dev/)
- [Flutter Cookbook](https://docs.flutter.dev/cookbook)
