import 'dart:convert';

import 'package:clearvue/models/Weather.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

class WeatherService {
  Future<Weather> fetchWeather() async {
    const apiKey = '07abb3d57b6cd2faae2bb6b3e9004b0e';
    const city = 'Mumbai';
    final response = await http.get(Uri.parse(
      'http://api.openweathermap.org/data/2.5/weather?q=$city&units=metric&appid=$apiKey',
    ));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      return Weather.fromJson(data);
    } else {
      throw Exception('Failed to load weather data');
    }
  }

  Future<List<WeatherForecast>> fetchWeatherForecast() async {
    const apiKey = '07abb3d57b6cd2faae2bb6b3e9004b0e';
    const city = 'Mumbai';
    final response = await http.get(Uri.parse(
      'http://api.openweathermap.org/data/2.5/forecast?q=$city&units=metric&appid=$apiKey',
    ));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      final List<dynamic> forecastList = data['list'];

      DateTime currentDate = DateTime.now();
      DateTime tomorrow =
          DateTime(currentDate.year, currentDate.month, currentDate.day + 1);

      Map<String, WeatherForecast> dailyForecasts = {};

      for (var forecastData in forecastList) {
        WeatherForecast weather = WeatherForecast.fromJson(forecastData);
        DateTime forecastDate =
            DateTime.parse(weather.forecastDate.replaceAll(" ", "T"));

        if (forecastDate.isAfter(currentDate) &&
            forecastDate.isBefore(tomorrow.add(const Duration(days: 5)))) {
          String dayKey = DateFormat('yyyy-MM-dd').format(forecastDate);

          if (!dailyForecasts.containsKey(dayKey) ||
              forecastDate.isBefore(DateTime.parse(
                  dailyForecasts[dayKey]!.forecastDate.replaceAll(" ", "T")))) {
            dailyForecasts[dayKey] = weather;
          }
        }
      }

      List<WeatherForecast> weatherList = dailyForecasts.values.toList();

      return weatherList;
    } else {
      throw Exception('Failed to load weather data');
    }
  }
}
