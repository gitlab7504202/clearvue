class Weather {
  final String city;
  final int temperature;
  final int weatherId;
  final String feelsLike;
  final String humidity;
  final String pressure;
  final String windSpeed;
  final int visibility;
  final String weatherDescription;

  Weather({
    required this.city,
    required this.weatherId,
    required this.temperature,
    required this.feelsLike,
    required this.humidity,
    required this.pressure,
    required this.windSpeed,
    required this.visibility,
    required this.weatherDescription,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      city: json['name'],
      visibility: json['visibility'] as int,
      temperature: (json['main']['temp'] as double).round(),
      feelsLike: json['main']['feels_like'].toString(),
      humidity: json['main']['humidity'].toString(),
      pressure: json['main']['pressure'].toString(),
      windSpeed: json['wind']['speed'].toString(),
      weatherDescription: json['weather'][0]['description'],
      weatherId: (json['id'] as int),
    );
  }
}

class WeatherForecast {
  final int dt;
  final String city;
  final double temperature;
  final int weatherId;
  final String feelsLike;
  final String humidity;
  final String pressure;
  final String windSpeed;
  final int visibility;
  final String weatherDescription;
  final String forecastDate;

  WeatherForecast({
    required this.city,
    required this.dt,
    required this.weatherId,
    required this.temperature,
    required this.feelsLike,
    required this.humidity,
    required this.pressure,
    required this.windSpeed,
    required this.visibility,
    required this.weatherDescription,
    required this.forecastDate,
  });

  factory WeatherForecast.fromJson(Map<String, dynamic> json) {
    try {
      return WeatherForecast(
        city: json['city']?['name'] ?? '',
        dt: json['dt'] ?? 0,
        visibility: json['visibility'] ?? 0,
        temperature: (json['main']?['temp'] as num?)?.toDouble() ?? 0,
        feelsLike: json['main']?['feels_like']?.toString() ?? '0',
        humidity: json['main']?['humidity']?.toString() ?? '0',
        pressure: json['main']?['pressure']?.toString() ?? '0',
        windSpeed: json['wind']?['speed']?.toString() ?? '0',
        weatherDescription: json['weather']?[0]?['description'] ?? '',
        weatherId: (json['weather']?[0]?['id'] as int?) ?? 0,
        forecastDate: json['dt_txt'] ?? '',
      );
    } catch (e) {
      print('Error parsing WeatherForecast: $e');
      return WeatherForecast(
        city: 'Mumbai',
        dt: 0,
        visibility: 0,
        temperature: 0,
        feelsLike: '0',
        humidity: '0',
        pressure: '0',
        windSpeed: '0',
        weatherDescription: '',
        weatherId: 0,
        forecastDate: '',
      );
    }
  }
}
