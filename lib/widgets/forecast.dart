import 'package:clearvue/models/Weather.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FiveDayForecastWidget extends StatelessWidget {
  final List<WeatherForecast> forecastList;
  final bool isDarkMode;

  const FiveDayForecastWidget({
    super.key,
    required this.forecastList,
    required this.isDarkMode,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: isDarkMode
            ? const Color.fromARGB(255, 61, 61, 61).withOpacity(0.25)
            : Colors.white54,
        borderRadius: BorderRadius.circular(20.0),
      ),
      padding: const EdgeInsets.all(20.0),
      margin: const EdgeInsets.only(top: 20.0),
      child: Column(
        children: forecastList.map((forecast) {
          DateTime forecastDate =
              DateTime.fromMillisecondsSinceEpoch(forecast.dt * 1000);
          bool isToday = DateTime.now().day == forecastDate.day;
          String dayOfWeek =
              isToday ? 'Today' : DateFormat('E').format(forecastDate);

          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                getIconImage(forecast.weatherId),
                Text(
                  dayOfWeek,
                  style: TextStyle(
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  '🌡️ ${forecast.temperature}°C',
                  style: TextStyle(
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  '💧 ${forecast.humidity}%',
                  style: TextStyle(
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  '🍃 ${forecast.windSpeed} m/s',
                  style: TextStyle(
                    color: isDarkMode ? Colors.white : Colors.black,
                  ),
                ),
              ],
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget getIconImage(int code) {
    switch (code) {
      case >= 200 && < 300:
        return Image.asset('assets/1.png', width: 30, height: 30);
      case >= 300 && < 400:
        return Image.asset('assets/2.png', width: 30, height: 30);
      case >= 500 && < 600:
        return Image.asset('assets/3.png', width: 30, height: 30);
      case >= 600 && < 700:
        return Image.asset('assets/4.png', width: 30, height: 30);
      case >= 700 && < 800:
        return Image.asset('assets/5.png', width: 30, height: 30);
      case == 800:
        return Image.asset('assets/6.png', width: 30, height: 30);
      case > 800 && <= 804:
        return Image.asset('assets/7.png', width: 30, height: 30);
      default:
        return Image.asset('assets/7.png', width: 30, height: 30);
    }
  }
}
